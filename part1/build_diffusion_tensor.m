function [a,b,d,coh,V2,Kf,Kb,W,cond] = build_diffusion_tensor(Ix,Iy,rho,alpha,beta,lambda1_method,lambda2_method,iter_num,Kf,Kb,W,...
    cond,num_for_cond)
%% Input:
% Ix,Iy - the horizontal and vertical derivatives of the image
% rho - the parameter for the width of the gaussian filter that is used for computing the average structural tensor
% alpha,beta - parameterw for lambda1 and lambda2 functions
% lambda1_method,lambda2_method - determine the functions used for  lambda1 and lambda2
% iter_num - the number of the current iteration
% num_for_cond - parameter for the decision if a block is directional
%
%% Output:
% a,b,d - the diffusion tensor for each pixel in the image (remember that c=b)
% coh - a matrix containing for each pixel the cohernce value
% V2 - a 2-layer matrix containing for each pixel a vector that is directed towards the coherence orientation
%
%% Input and Output:
% Kf,Kb,W - the FAB coefficients for each pixel in the image 
% cond - a binary matrix containing for each pixel '0' if it is in a directional block and '1' otherwise  
%
%% compute the coherence value and orientation for each pixel in in the image
[J11,J12,J22] = compute_structural_tensor(Ix,Iy,rho);
[coh,V1,V2] = compute_coherence_and_orientations(J11,J12,J22);
coh_med=median(log10(1+coh(:)));
%% determine lambda1
if strcmp(lambda1_method,'global')
    lambda1=alpha;
elseif strcmp(lambda1_method,'negative')
    lambda1=-beta;
elseif strcmp(lambda1_method(1:3),'FAB')
    I_mag=0;
    for i=1:size(Ix,3)
        I_mag=I_mag+sqrt(Ix(:,:,i).^2+Iy(:,:,i).^2)/sqrt(2);
    end
    if size(Ix,3)>1
        I_mag=I_mag/size(Ix,3);
    end
    if iter_num==1   
        [Kf,Kb,W]=calc_FAB_coeffs(I_mag,64,0);
    end
    if strcmp(lambda1_method(5:end),'only_backward')
        lambda1=-beta./(1+((I_mag-Kb)./W).^4); %for superresolution 
    elseif strcmp(lambda1_method(5:end),'only_forward')
        lambda1=1./(1+(I_mag./Kf).^4); %"standard" PM coefficient
    elseif strcmp(lambda1_method(5:end),'standard') 
        lambda1=1./(1+(I_mag./Kf).^4)-beta./(1+((I_mag-Kb)./W).^4); %"standard" FAB coefficient 
    end
end
%
%% determine for each block if it is directional (for the 'log_coh_cond' method)
if strcmp(lambda2_method,'log_coh_cond')
    if iter_num==1
       cond = determine_if_directional(coh,V2,num_for_cond);
    end
end
%
%% determine lambda2
if strcmp(lambda2_method,'global')
    lambda2=1;
elseif strcmp(lambda2_method,'log_coh_cond')
    lambda2=alpha+(1-alpha)*(1-cond)./(1+(coh_med./log10(1+coh)).^4);
elseif strcmp(lambda2_method,'log_coh')
    lambda2=alpha+(1-alpha)./(1+(coh_med./log10(1+coh)).^4);
end
%
%% compute the diffusion tensor
a=lambda1.*V1(:,:,1).^2+lambda2.*V2(:,:,1).^2;
b=lambda1.*V1(:,:,1).*V1(:,:,2)+lambda2.*V2(:,:,1).*V2(:,:,2);
d=lambda1.*V1(:,:,2).^2+lambda2.*V2(:,:,2).^2;
%
end
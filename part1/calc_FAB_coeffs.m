function [Kf,Kb,W] = calc_FAB_coeffs(I_mag,num_wins,is_denoising)
%% Input:
% I_mag - the gradient magnitude for each pixel in the image
% num_wins - the number of windows for the block partition. The FAB coefficients are computed for each block separately 
% is_denoising - FAB is used for denoising
%
%% Output:
% Kf,Kb,W - the FAB coefficients for each pixel in the image 
%
%% compute the FAB coefficients for each pixel in the image
per1=30; 
per2=90; 
[Ny,Nx]=size(I_mag); 
L_col=floor(Nx/sqrt(num_wins));
L_row=floor(Ny/sqrt(num_wins));
for g=1:sqrt(num_wins)
    for h=1:sqrt(num_wins)
        start_row=1+(g-1)*L_row;
        end_row=g*L_row;
        start_col=1+(h-1)*L_col;
        end_col=h*L_col;
        if g==sqrt(num_wins)
            end_row=size(I_mag,1);
        end
        if h==sqrt(num_wins)
            end_col=size(I_mag,2);
        end
        win=I_mag(start_row:end_row,start_col:end_col);
        mean_win=mean(win(:));
        I_mag_avg(start_row:end_row,start_col:end_col)=mean_win*ones(size(win));
        [num_hist,xout] = hist(win(win(:)>mean_win),100);
        per_hist=num_hist/sum(num_hist(:));
        accum_per=100*cumsum(per_hist);
        [m1,ind1]=min(abs(accum_per-per1));
        [m2,ind2]=min(abs(accum_per-per2));
        Kb(start_row:end_row,start_col:end_col)=(xout(ind2)+xout(ind1))/2*ones(size(win));
        W(start_row:end_row,start_col:end_col)=Kb(start_row:end_row,start_col:end_col)-xout(ind1);
    end
end
if ~is_denoising
    Kf=I_mag_avg;
else
    Kf=10*ones(size(I_mag_avg));
end
%
end
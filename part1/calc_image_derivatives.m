function [Ix,Iy] = calc_image_derivatives(I,filter_type )
%% Input:
% I - the image 
% filter_type - the method for calculating the derivative
%
%% Output:
% Ix,Iy - the horizontal and vertical derivatives of the image
%
%% compute horizontal and vertical derivatives of the image
if strcmp(filter_type,'central_derivative')
    Fx=1/2*[0 0 0; -1 0 1; 0 0 0];
elseif strcmp(filter_type,'rotation_invariant')
    Fx=1/32*[-3 0 3; -10 0 10; -3 0 3];
end
Fy=-Fx';
Ix=filter2(Fx,I ,'same');
Iy=filter2(Fy,I ,'same');
Ix(1:end,1)=0;
Ix(1:end,end)=0;
Ix(1,1:end)=0;
Ix(end,1:end)=0;
Iy(1:end,1)=0;
Iy(1:end,end)=0;
Iy(1,1:end)=0;
Iy(end,1:end)=0;
%
end
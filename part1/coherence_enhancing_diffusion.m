function [Id,PSNR] = coherence_enhancing_diffusion(I,I_org,N,dt,sigma2,rho,alpha,beta,lambda1_method,lambda2_method,filter_type,...
    draw_coh_directions,optimal,PSNR_thr,num_for_cond,show_directional_blocks)
%% Input:
% I - The image on which we apply the diffusion process (initial condition for the process)
% I_org - The original image that I was generated from (for example - if I is a noisy image, I_org is the noiseless image)
% N - maximal number of iterations in the diffusion process
% dt - size of time step for each iteration
% sigma2 - the variance of the gaussian filter for calculating a smoothed version of the image
% rho - the parameter for the width of the gaussian filter that is used for computing the average structural tensor
% alpha,beta - parameterw for lambda1 and lambda2 functions
% lambda1_method,lambda2_method - determine the functions used for  lambda1 and lambda2
% options for lambda1_method: 'global', 'negative', 'FAB_only_forward', 'FAB_standard', 'FAB_only_backward' 
% options for lambda2_method: 'global', 'log_coh', 'log_coh_cond' 
% filter_type - the method for calculating the derivative
% options for filter_type: 'central_derivative', 'rotation_invariant'
% optimal - the condition on the PSNR for stopping the process:
% optimal=1 - stop the process when maximal PSNR is reached
% optimal=2 - stop the process when a pre-determined threshold is reached during  the decline stage of the PSNR 
% num_for_cond - parameter for the decision if a block is directional
% PSNR_thr - the threshold value for the PSNR used in the case of optimal=2
% draw_coh_directions, show_directional_blocks - display flags
%
%% Output:
%Id - the final image after the diffusion process
% PSNR - the PSNR for each iteration in the diffusion process
%
%% determine all necessary inputs
if ~exist('N','var')
    N=1;
end
if ~exist('dt','var')
    dt=0.125;
end
if ~exist('sigma2','var')
    sigma2=0;
end
if ~exist('rho','var')
    rho=2;
end
if ~exist('alpha','var')
    alpha=0.001;
end
if ~exist('beta','var')
    beta=0.1;
end
if ~exist('lambda2_method','var')
    lambda1_method='global';
end
if ~exist('lambda2_method','var')
    lambda2_method='global';
end
if ~exist('filter_type','var')
    filter_type='central_derivative';
end
if ~exist('draw_coh_directions','var')
    draw_coh_directions=0;
end
if ~exist('optimal','var')
    optimal=0;
end
if ~exist('PSNR_thr','var')
    PSNR_thr=27;
end
if ~exist('num_for_cond','var')
    num_for_cond=5;
end
if ~exist('show_directional_blocks','var')
    show_directional_blocks=0;
end
%
%%  main diffusion iteration
num_channels=size(I,3);
I0=I;
Kf=[];
Kb=[];
W=[];
cond=[];
for i=1:N
    for j=1:num_channels
        [I0x(:,:,j),I0y(:,:,j)]=calc_image_derivatives(I0(:,:,j),filter_type); %I0x,I0y are derivatives of the image before smoothing it
        if (sigma2>0)
            h_gauss=fspecial('gaussian',3,sqrt(sigma2));  % gaussian filter with kernel 3x3
            I1=imfilter(I0(:,:,j),h_gauss,'replicate'); % I1 is a smoothed version of the image
            [I1x(:,:,j),I1y(:,:,j)]=calc_image_derivatives(I1,filter_type); %I1x,I1y are derivatives of the smoothed image
        end
    end
    if (sigma2==0)
        I1x=I0x;
        I1y=I0y;
    end
    % calculate the diffusion tensor according to the method
    [a,b,d,coh,V2,Kf,Kb,W,cond] = build_diffusion_tensor(I1x,I1y,rho,alpha,beta,lambda1_method,lambda2_method,i,Kf,Kb,W,cond,num_for_cond);
    for j=1:num_channels
        J1=a.*I0x(:,:,j)+b.*I0y(:,:,j); 
        J2=b.*I0x(:,:,j)+d.*I0y(:,:,j);
        [J1x,J1y]=calc_image_derivatives(J1,filter_type);
        [J2x,J2y]=calc_image_derivatives(J2,filter_type);
        I0(:,:,j)=I0(:,:,j)+dt*(J1x+J2y); % calculate the next iteration image
    end
    % calculate the PSNR for each iteration
    MSE=mean((I_org(:)-I0(:)).^2);
    PSNR(i)=10*log10(255^2/MSE);
    % if a specific condition on the PSNR is reached, stop the diffusion process
    if optimal==1
        if PSNR(i)<max(PSNR(:))-0.1
            break;
        end
    elseif optimal==2
        if i>2
            if abs(PSNR(i)-PSNR_thr)<0.2 && PSNR(i)<PSNR(i-1)
                break;
            end
        end
    end
    % display directional blocks (for the 'log_coh_cond' method) 
    if strcmp(lambda2_method(end-3:end),'cond') && show_directional_blocks
        if i==1
            H=I_org;
            for k=1:size(H,3)
                G=H(:,:,k);
                G(cond==1)=0; %all undirectional blocks are marked in black 
                H(:,:,k)=G;
            end
            figure,imshow(uint8(H))
        end
    end
    % display the coherence orientations in the beginning and end of the diffusion process 
    if draw_coh_directions 
        if i==1 || i==N
            m=mean(coh(:));
            figure,quiver(flipud(V2(1:4:end,1:4:end,1).*(coh(1:4:end,1:4:end)>m)),flipud(V2(1:4:end,1:4:end,2).*(coh(1:4:end,1:4:end)>m)));
        end
    end
end
Id=I0;
%
end
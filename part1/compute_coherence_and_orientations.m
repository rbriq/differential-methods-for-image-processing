function [coh,V1,V2] = compute_coherence_and_orientations(J11,J12,J22)
%% Input:
% J11,J12,J22 - the average structural tensor (remember that J21=J12)
%
%% Output:
% coh - a matrix containing for each pixel the cohernce value
% V1 - a 2-layer matrix containing for each pixel a vector that is directed towards the edge orientation
% V2 - a 2-layer matrix containing for each pixel a vector that is directed towards the coherence orientation
%
%% compute the coherence value and the coherence and edge orientations for each pixel in the image
delta=sqrt(((J11-J22).^2+4*J12.^2));
coh=delta.^2;
u1=0.5*(J11+J22+delta);
cond_for_V=abs(J12)>=1;
V11=zeros(size(u1));
V11(cond_for_V)=sqrt(1./(1+((u1(cond_for_V)-J11(cond_for_V))./J12(cond_for_V)).^2));
V1(:,:,1)=V11;
V12=zeros(size(u1));
V12(cond_for_V)=(-1+(2*(((u1(cond_for_V)-J11(cond_for_V))./J12(cond_for_V))>0))).*sqrt(1-V11(cond_for_V).^2);
V1(:,:,2)=V12;
V2(:,:,1)=V1(:,:,2);
V2(:,:,2)=-V1(:,:,1);
[row,col]=find(~cond_for_V);
for i=length(row) 
    J=[J11(row(i),col(i)),J12(row(i),col(i));J12(row(i),col(i)),J22(row(i),col(i))];
    [V,Q]=eig(J);
    if Q(1,1)<Q(2,2)
        V=[V(:,2),V(:,1)];
    end
    V1(row(i),col(i),:)=V(:,1);
    V2(row(i),col(i),:)=V(:,2);
end
%
end
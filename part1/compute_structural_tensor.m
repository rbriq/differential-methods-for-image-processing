function [J11,J12,J22] = compute_structural_tensor(Ix,Iy,rho)
%% Input:
% Ix,Iy - the horizontal and vertical derivatives of the image
% rho - the parameter for the width of the gaussian filter that is used for computing the average structural tensor
%
%% Output:
% J11,J12,J22 - the average structural tensor (remember that J21=J12)
%
%% compute the structural tensor for each pixel in the image
size_win=1+4*rho;
J11=0;
J12=0;
J22=0;
for i=1:size(Ix,3)
    J11=J11+Ix(:,:,i).^2;
    J12=J12+Ix(:,:,i).*Iy(:,:,i);
    J22=J22+Iy(:,:,i).^2;
end
if size(Ix,3)>1
    J11=J11+1;
    J22=J22+1;
end
h_gauss=fspecial('gaussian',size_win,sqrt(rho));  
J11=imfilter(J11,h_gauss,'replicate');
J12=imfilter(J12,h_gauss,'replicate');
J22=imfilter(J22,h_gauss,'replicate');
%
end
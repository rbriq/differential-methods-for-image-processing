function [cond] = determine_if_directional(coh,V2,num_for_cond)
%% Input:
% coh - a matrix containing the coherence magnitude for each pixel
% V2 - a 2-layer matrix containing for each pixel a vector that is directed towards the coherence orientation
% num_for_cond - parameter for the decision if a block is directional
%
%% Output:
% cond - a binary matrix containing for each pixel '0' if it is in a directional block and '1' otherwise  
%
%% decide for each pixel in the image if it is located in a directional block
teta=atand(V2(:,:,2)./V2(:,:,1));
[Ny,Nx]=size(teta);
blk_size=16;
row_ind=1:blk_size:Ny;
col_ind=1:blk_size:Nx;
for g=1:length(row_ind)-1
    for h=1:length(col_ind)-1
        start_row=row_ind(g);
        end_row=start_row+blk_size-1;
        start_col=col_ind(h);
        end_col=start_col+blk_size-1;
        if g==length(row_ind)-1
            end_row=size(teta,1);
        end
        if h==length(col_ind)-1
            end_col=size(teta,2);
        end
        teta_win=teta(start_row:end_row,start_col:end_col);
        coh_win=coh(start_row:end_row,start_col:end_col);
        teta_win_large_coh=teta_win(coh_win>2*mean(coh_win(:)));
        if isempty(teta_win_large_coh)
            teta_win_large_coh=teta_win(coh_win>mean(coh_win(:)));
        end
        teta_var1(start_row:end_row,start_col:end_col)=var(teta_win_large_coh)*ones(size(teta_win));
        teta_var2(start_row:end_row,start_col:end_col)=var(teta_win_large_coh+180*(teta_win_large_coh<0))*ones(size(teta_win));
    end
end
teta_var=min(teta_var1,teta_var2);
cond=sqrt(teta_var)>num_for_cond;
%
end
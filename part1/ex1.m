clc;
close all;
clear all;

%I1 = im2double(rgb2gray(imread('Paignton_Shops.jpg')));
I1 = im2double(imread('Paignton_Shops.jpg'));
figure(1); 
imshow(I1);
title('original image');
sigma = 0.005;
Inoised1 = imnoise(I1 ,'gaussian', 0, sigma);

figure(2); 
imshow(Inoised1);
title('noised image');

dt = 0.125;
N = 100;
beta = 0.1;
K = 10;
sigma2 = 0.5;


[Id,PSNR] = non_linear_diffusion( 255 * Inoised1,255 * I1,'PM',N,dt,sigma2,K,beta); %,0,27, 0 );

figure(3);
imshow(Id/255);
title('image after non linear diffusion IN PM method and optimal = 0');

t = 1:N;
figure(4);
plot(t, PSNR);
title('PSNR in PM  method with iterations number = 100 iterations');
xlabel('t');
ylabel('PSNR');


[Id,PSNR] = non_linear_diffusion( 255 * Inoised1,255 *I1,'FAB',N,dt,sigma2,K,beta); %,0,27, 0 );

figure(5);
plot(t, PSNR);
title('PSNR in FAB method with iterations number  = 100iterations');
xlabel('t');
ylabel('PSNR');

%part vav
[Id1,PSNR1] = non_linear_diffusion( 255 * Inoised1,255 *I1,'PM',N,dt,sigma2,K,beta,1);
[Id2,PSNR2] = non_linear_diffusion( 255 * Inoised1,255 *I1,'FAB',N,dt,sigma2,K,beta,1);
figure(6);
imshow(Id1/255);
title('image after non linear diffusion IN PM method and optimal = 1');
figure(7);
imshow(Id2/255);
title('image after non linear diffusion IN FAB method and optimal = 1');
maxPSNR1 = max(PSNR1);
maxPSNR2 = max(PSNR2);
PSNRThresh = 23;
figure (8);
subplot(2,1,1);
t = 1:length(PSNR1);
plot(t, PSNR1);
subplot(2,1,2);
t = 1:length(PSNR2);
plot(t, PSNR2);
title('comparing PSNR IN PM method vs PSNR in FAB method');



%PART ZAYEN
PSNR_thresh = 29;
[Id1,PSNR1] = non_linear_diffusion( 255 * Inoised1,255 *I1,'PM',N,dt,sigma2,K,beta,2, PSNR_thresh);
[Id2,PSNR2] = non_linear_diffusion( 255 * Inoised1,255 *I1,'FAB',N,dt,sigma2,K,beta,2, PSNR_thresh);
figure(9);
imshow(Id1/255);
title('image after non linear diffusion IN PM method and optimal = 2');
figure(10);
imshow(Id2/255);
title('image after non linear diffusion IN FAB method and optimal = 2');
maxPSNR1 = max(PSNR1)
maxPSNR2 = max(PSNR2)
figure (14);
subplot(2,1,1);
t = 1:length(PSNR1);
plot(t, PSNR1);
subplot(2,1,2);
t = 1:length(PSNR2);
plot(t, PSNR2);
title('comparing PSNR IN PM method vs PSNR in FAB method');



%PART HET

dt = 0.5;

[Id1,PSNR1] = non_linear_diffusion( 255 * Inoised1,255 *I1,'PM',N,dt,sigma2,K,beta);
[Id2,PSNR2] = non_linear_diffusion( 255 * Inoised1,255 *I1,'FAB',N,dt,sigma2,K,beta);

figure(11);
imshow(Id1/255);
title('image after non linear diffusion IN PM method and dt = 0.5');
figure(12);
imshow(Id2/255);
title('image after non linear diffusion IN FAB method and dt = 0.5');
maxPSNR1 = max(PSNR1);
maxPSNR2 = max(PSNR2);

figure (13);
subplot(2,1,1);
t = 1: length(PSNR1);
plot(t, PSNR1);
subplot(2,1,2);
t = 1:length(PSNR2);
plot(t, PSNR2);
title('comparing PSNR in PM method vs PSNR in FAB method with dt = 0.3');


maxPSNR1 = max(PSNR1)
maxPSNR2 = max(PSNR2)




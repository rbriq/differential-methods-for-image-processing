clc;
close all;
clear all;

I1 = im2double(imread('fingerprint.jpg'));
figure(1); 
%subplot(2,1,1);
imshow(I1);
title('original image');

sigma = 0.005;
Inoised1 = imnoise(I1 ,'gaussian', 0, sigma);
figure(2); 
imshow(Inoised1);
title('noised image');

dt = 0.125;
N = 72;
beta = 0.1;
rho = 6;
alpha = 0.001;
sigma2 = 0.5;

[Id,PSNR] = coherence_enhancing_diffusion(255 * Inoised1, 255 * I1, N,dt,sigma2,rho,alpha,beta,'global','global','rotation_invariant');
figure(3); 
imshow(Id/255);
title('image after directional coherence enhancing diffusion');

%part bet

I2 = im2double(imread('tree.jpg'));
figure(4); 
imshow(I2);
title('original image');

sigma = 0.005;
Inoised2 = imnoise(I2 ,'gaussian', 0, sigma);
figure(5); 
imshow(Inoised2);
title('noised image');

[Id,PSNR] = coherence_enhancing_diffusion(255 * Inoised2,255 * I2,N,dt,sigma2,rho,alpha,beta,'global','global','rotation_invariant', 1);

figure(6);
imshow(Id/255);
title('image after directional coherence enhancing diffusion and coherence directions');

%part gimel

[x,y]=meshgrid(1:0.5:401,1:0.5:401);
R=(x-201).^2+(y-201).^2;
I3=255*abs(sin(R/100));
I3=I3(401:601,201:401);
figure(7); 
imshow(I3/255);
title('original image');

Inoised3 = imnoise(I3/255 ,'gaussian', 0, sigma);
figure(8); 
imshow(Inoised3);
title('noised image');
%N = 100;
%dt = 0.125;
sigma2 = 0.5;
K = 10;
beta = 0.1;
[Id3,PSNR3] = non_linear_diffusion( 255 * Inoised3,255 * I3,'PM',N,dt,sigma2,K,beta,1);

figure(9); 
imshow(Id3/255);
title('image after non-linear diffusion and optimal = 1');
t = 1:length(PSNR3);
figure(10);
plot(t, PSNR3);

rho = 4;

[Id,PSNR] = coherence_enhancing_diffusion(255 * Inoised3,255 * I3,N,dt,sigma2,rho,alpha,beta,'global','global','rotation_invariant');
figure(11); 
imshow(Id/255);
title('image after directional coherence enhancing diffusion and \rho = 4 and rotation_invariant');

t = 1:length(PSNR);
figure(12);
plot(t, PSNR);
title('PSNR after directional coherence enhancing diffusion and \rho = 4 and rotation_invariant');

[Id,PSNR] = coherence_enhancing_diffusion(255 * Inoised3,255 * I3,N,dt,sigma2,rho,alpha,beta,'global','global','central_derivative');
figure(13); 
imshow(Id/255);
title('image after directional coherence enhancing diffusion and rho = 4 and central derivative');
t = 1:length(PSNR);
figure(14);
plot(t, PSNR);
title('PSNR after directional coherence enhancing diffusion and rho = 4 and central derivative');


%part d  

I4 = im2double(imread('wall.jpg'));
figure(15); 
%subplot(2,1,1);
imshow(I4);
title('original image');

sigma = 0.005;
Inoised4 = imnoise(I4 ,'gaussian', 0, sigma);
figure(16); 
imshow(Inoised4);
title('noised image');

N = 48;
rho = 2;

[Id1,PSNR1] = coherence_enhancing_diffusion(255 * Inoised4, 255 * I4,N,dt,sigma2,rho,alpha,beta,'global','global','central_derivative');

figure(17); 
imshow(Id1/255);
title('image after coherence enhancing diffusion and \rho = 2 and global method ');

rho = 10;
[Id2,PSNR2] = coherence_enhancing_diffusion(255 * Inoised4, 255 * I4,N,dt,sigma2,rho,alpha,beta,'global','global','central_derivative');

figure(18); 
imshow(Id2/255);
title('image after coherence enhancing diffusion and \rho = 10 and global method ');

t = 1:length(PSNR1);
figure(19);
plot(t, PSNR1);
title('PSNR after coherence enhancing diffusion and \rho = 2 and global method ');

t = 1:length(PSNR2);
figure(20);
plot(t, PSNR2);
title('PSNR after coherence enhancing diffusion and rho = 10 and global method ');


[Id1,PSNR1] = coherence_enhancing_diffusion(255 * Inoised4,255 * I4,N,dt,sigma2,rho,alpha,beta,'negative','global','central_derivative');
rho = 2;
[Id2,PSNR2] = coherence_enhancing_diffusion(255 * Inoised4,255 * I4,N,dt,sigma2,rho,alpha,beta,'negative','global','central_derivative');

figure(21); 
imshow(Id1/255);
title('image after coherence enhancing diffusion and \rho = 10 and \lambda_1 method = negative ');

t = 1:length(PSNR1);
figure(22);
plot(t, PSNR1);
title('PSNR after coherence enhancing diffusion and \rho = 10 and \lambda_1 method = negative ');

figure(23); 
imshow(Id2/255);
title('image after coherence enhancing diffusion and \rho = 2 and \lambda_1 method = negative ');

t = 1:length(PSNR2);
figure(24);
plot(t, PSNR2);
title('PSNR after coherence enhancing diffusion and \rho = 2 and \lambda_1 method = negative ');

%%%%%%%%%%%%%%%%%%
% experiment3
%%%%%%%%%%%%%%%%%%
close all;
clc;
clear all;

I1 = im2double((imread('wizard.jpg')));
imwrite(im2uint8(I1), 'wizard_original.jpg', 'JPEG');
% figure(1);
% imshow(I1);
%title('original image');
scale = 3;
scaledI1 = imresize(I1, 1/scale, 'bicubic');
imwrite(im2uint8(scaledI1), 'wizard_scaled3.jpg', 'JPEG');

% figure(2);
% imshow(scaledI1);
% title('image scaled by a scale of 1/3');

% part a: peforming super resolution using bicubic interpolation


rescaledI1 = imresize(scaledI1, scale, 'bicubic');
imwrite(im2uint8(rescaledI1), 'wizard_LQ.jpg', 'JPEG');
% figure(3);
% imshow(rescaledI1);
% title('image rescaled by a scale of 3 from scaled image');

%part b: peforming super resolution using the non linear diffusion method

sigma2 = 0.5;
K = 10;
beta = 0.1;
dt = 0.125;
N = 48;
rescaledI11 = rescaledI1(1:803,:); %in order to match the demisions of the original image

[Id,PSNR] = non_linear_diffusion( 255 * rescaledI11, 255 * I1,'FAB',N,dt,sigma2,K,beta,0, 27,1);

% figure(4);
% imshow(Id);
% title('Interpolated image using non linear diffusion');
imwrite(im2uint8(Id/255), 'wizard_SR1.jpg', 'JPEG');

%part c 1
rho = 6;
alpha = 0.001;
beta = 0.1;

[Id0,PSNR] = coherence_enhancing_diffusion(255 * rescaledI11,255 * I1,N,dt,sigma2,rho,alpha,beta,'negative','global', 'rotation_invariant');
imwrite(im2uint8(Id0/255), 'wizard_SR2.jpg', 'JPEG');
%part c 2

[Id,PSNR] = coherence_enhancing_diffusion(255 * rescaledI11,255 * I1,N,dt,sigma2,rho,alpha,beta,'FAB_only_backward','log_coh_cond', 'rotation_invariant');

imwrite(im2uint8(Id/255), 'wizard_SR3.jpg', 'JPEG');
%imshow(Id);
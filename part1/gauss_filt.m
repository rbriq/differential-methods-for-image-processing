function [Ig] = gauss_filt(I,c,N,dt)
t=N*dt;
gauss_sigma=sqrt(2*c*t);
hsize=min(2*N+1,size(I));
h=fspecial('gaussian',hsize,gauss_sigma);
Ig=imfilter(I,h,'replicate');
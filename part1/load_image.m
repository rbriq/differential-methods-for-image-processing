function [I] = load_image(pic_name,color)
%% Input:
% pic_name - full path for the image to be loaded from
% colour: 0 - convert to grayscale image
%
%% Output:
% I - the image prepared for work
%
%% load image and prepare it 
I=imread(pic_name);
if size(I,3)==3 && ~color
    I=rgb2gray(I); % convert to grayscale image
end
I=double(I); % convert to double so we can perform filtering on it
%
end
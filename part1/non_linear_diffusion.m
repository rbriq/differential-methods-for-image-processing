function [Id,PSNR]=non_linear_diffusion(I,I_org,method,N,dt,sigma2,K,beta,optimal,PSNR_thr,is_SR)
%% Input:
% I - The image on which we apply the diffusion process (initial condition for the process)
% I_org - The original image that I was generated from (for example - if I is a noisy image, I_org is the noiseless image)
% N - maximal number of iterations in the diffusion process
% dt - size of time step for each iteration
% sigma2 - the variance of the gaussian filter for calculating a smoothed version of the image
% K - the gradient magnitude that corresponds to 0.5 in the PM diffusion coefficient
% beta - ratio between backward and forward components in the FAB diffusion coefficient
% is_SR - the non linear diffusion is used for single image super resolution 
% optimal - the condition on the PSNR for stopping the process:
% optimal=0 - stop the process when pre-determined number of iterations is reached
% optimal=1 - stop the process when maximal PSNR is reached
% optimal=2 - stop the process when a pre-determined threshold is reached during  the decline stage of the PSNR 
% PSNR_thr - the threshold value for the PSNR used in the case of optimal=2
%
%% Output:
%Id - the final image after the diffusion process
% PSNR - the PSNR for each iteration in the diffusion process 
%
%% determine all necessary inputs
if (nargin<3)
    error('not enough arguments (at least 3 should be given)');
end
if ~exist('N','var')
    N=1; %one diffusion step
end
if ~exist('dt','var')
    dt=0.125;
end
if ~exist('sigma2','var')
    sigma2=0;
end
if ~exist('K','var')
    K=1;
end
if ~exist('beta','var')
    beta=0.1;
end
if ~exist('optimal','var')
    optimal=0; %No condition on the PSNR
end
if ~exist('PSNR_thr','var')
    PSNR_thr=27; 
end
if ~exist('is_SR','var')
    is_SR=0;
end
%
%% main diffusion iteration
I0=I; %The initial condition for the diffusion process is I
[Ny,Nx,num_channels]=size(I0);
I_mag=0;
for i=1:N
    for j=1:num_channels
        if (sigma2>0)
            h_gauss=fspecial('gaussian',3,sqrt(sigma2));  % gaussian filter with kernel 3x3
            I1=imfilter(I0(:,:,j),h_gauss,'replicate'); % I1 is a smoothed version of the image
        else
            I1=I0(:,:,j);
        end
        % calculate gradient in all directions (N,S,E,W)
        In(:,:,j)=[I1(1,:); I1(1:Ny-1,:)]-I1;
        Is(:,:,j)=[I1(2:Ny,:); I1(Ny,:)]-I1;
        Ie(:,:,j)=[I1(:,2:Nx) I1(:,Nx)]-I1;
        Iw(:,:,j)=[I1(:,1) I1(:,1:Nx-1)]-I1;
        if i==1
            I_mag=I_mag+sqrt((In(:,:,j)-Is(:,:,j)).^2+(Iw(:,:,j)-Ie(:,:,j)).^2)/2;
        end
    end
    abs_In=abs(In);
    abs_Is=abs(Is);
    abs_Ie=abs(Ie);
    abs_Iw=abs(Iw);
    if i==1
        I_mag=I_mag/num_channels;
    end
    if strcmp(method,'FAB')
        if i==1
            is_denoising=(sigma2>0);
            [Kf,Kb,W]=calc_FAB_coeffs(I_mag,64,is_denoising);
        end
    end
    for j=1:num_channels
        % calculate diffusion coefficients in all directions according to the method
        if strcmp(method,'PM')
            Cn=1./(1+(abs_In(:,:,j)/K).^4);
            Cs=1./(1+(abs_Is(:,:,j)/K).^4);
            Ce=1./(1+(abs_Ie(:,:,j)/K).^4);
            Cw=1./(1+(abs_Iw(:,:,j)/K).^4);
        elseif strcmp(method,'FAB')
            if ~is_SR
                Cn=1./(1+(abs_In(:,:,j)./Kf).^4)-beta./(1+((abs_In(:,:,j)-Kb)./W).^4);
                Cs=1./(1+(abs_Is(:,:,j)./Kf).^4)-beta./(1+((abs_Is(:,:,j)-Kb)./W).^4);
                Ce=1./(1+(abs_Ie(:,:,j)./Kf).^4)-beta./(1+((abs_Ie(:,:,j)-Kb)./W).^4);
                Cw=1./(1+(abs_Iw(:,:,j)./Kf).^4)-beta./(1+((abs_Iw(:,:,j)-Kb)./W).^4);
            else
                Cn=-beta./(1+((abs_In(:,:,j)-Kb)./W).^4);
                Cs=-beta./(1+((abs_Is(:,:,j)-Kb)./W).^4);
                Ce=-beta./(1+((abs_Ie(:,:,j)-Kb)./W).^4);
                Cw=-beta./(1+((abs_Iw(:,:,j)-Kb)./W).^4);
            end
        else
            error(['Unknown method "' method '"']);
        end
        if (sigma2>0)   % calculate real gradients (not smoothed)
            In0=[I0(1,:,j); I0(1:Ny-1,:,j)]-I0(:,:,j);
            Is0=[I0(2:Ny,:,j); I0(Ny,:,j)]-I0(:,:,j);
            Ie0=[I0(:,2:Nx,j) I0(:,Nx,j)]-I0(:,:,j);
            Iw0=[I0(:,1,j) I0(:,1:Nx-1,j)]-I0(:,:,j);
        else
            In0=In(:,:,j);
            Is0=Is(:,:,j);
            Ie0=Ie(:,:,j);
            Iw0=Iw(:,:,j);
        end
        I0(:,:,j)=I0(:,:,j)+dt*(Cn.*In0 + Cs.*Is0 + Ce.*Ie0 + Cw.*Iw0);   % calculate the next iteration image
    end
    % calculate the PSNR for each iteration
    MSE=mean((I_org(:)-I0(:)).^2);
    PSNR(i)=10*log10(255^2/MSE);
    % if a specific condition on the PSNR is reached, stop the diffusion process 
    if optimal==1
        if PSNR(i)<max(PSNR(:))-0.1
            break;
        end
    elseif optimal==2
        if i>2
            if abs(PSNR(i)-PSNR_thr)<0.2 && PSNR(i)<PSNR(i-1)
                break;
            end
        end
    end
end 
Id=I0; 
%
end
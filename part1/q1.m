%pre1_q1
clear all;
I=rgb2gray(imread('house.jpg'));
imshow(I);
title('house before filtering')
average=mean(mean(I));

N=10;
I_filterd1=gauss_filt(I,1,N,0.1);
imshow(I_filterd1)
title('house after filtering with N=10 ')
average1=mean(mean(I_filterd1));

N=100;
I_filterd2=gauss_filt(I,1,N,0.1);
imshow(I_filterd2)
title('house after filtering with N=100')
average2=mean(mean(I_filterd2));

N=500;
I_filterd3=gauss_filt(I,1,N,0.1);
imshow(I_filterd3)
title('house after filtering with N=500 ')
average3=mean(mean(I_filterd3));


%q1
clear all
%a
I_org= double(rgb2gray(imread('paignton_shops.jpg')));

I=double(255*imnoise(I_org/255,'gaussian',0,0.005));

%b
[Id,PSNR]=non_linear_diffusion(I,I_org,'PM',100,0.125,0.5,10,0.1);
plot()
%c

clear all

%q2
%a
I=double(imread('fingerprint.jpg'));
imshow(mat2gray(I))
title('original fingerprint')

[Id,PSNR] = coherence_enhancing_diffusion(I,I,72,0.125,0.5,6,0.001,0.1,'global','global','rotation_invariant');
imshow(mat2gray(Id))
title(' fingerprint aftercoherence enhancing diffusion' );


